mobs_doomed = {}

-- dmobs by D00Med

-- mounts api by D00Med and lib_mount api by blert2112

dofile(minetest.get_modpath("mobs_doomed").."/api.lua")

-- Enable dragons (disable to remove tamed dragons and dragon bosses)
mobs_doomed.dragons = minetest.settings:get_bool("mobs_doomed.dragons")
if mobs_doomed.dragons == nil then mobs_doomed.dragons = true end

-- Enable regular mobs independient of dragons (if all disabled one dragon and some orcs will be enabled)
mobs_doomed.regulars = minetest.settings:get_bool("mobs_doomed.regulars")
if mobs_doomed.regulars == nil then mobs_doomed.regulars = true end

-- Enabling of NyanCat
mobs_doomed.allow_nyanc = minetest.settings:get_bool("mobs_doomed.allow_nyanc")
if mobs_doomed.allow_nyanc == nil then mobs_doomed.allow_nyanc = false end

-- Enable fireballs/explosions
mobs_doomed.destructive = minetest.settings:get_bool("mobs_doomed.destructive")
if mobs_doomed.destructive == nil then mobs_doomed.destructive = true end

-- Timer for the egg mechanics
mobs_doomed.eggtimer = tonumber(minetest.settings:get("mobs_doomed.eggtimer") )
if mobs_doomed.eggtimer == nil then mobs_doomed.eggtimer = 100 end



-- Table cloning to reduce code repetition
mobs_doomed.deepclone = function(t) -- deep-copy a table -- from https://gist.github.com/MihailJP/3931841
	if type(t) ~= "table" then return t end

	local target = {}

	for k, v in pairs(t) do
		if k ~= "__index" and type(v) == "table" then -- omit circular reference
			target[k] = mobs_doomed.deepclone(v)
		else
			target[k] = v
		end
	end
	return target
end

-- Start loading ----------------------------------------------------------------------------------

local function loadmob(mobname,dir)
	dir = dir or "/mobs/"
	dofile(minetest.get_modpath("mobs_doomed")..dir..mobname..".lua")
end

-- regular mobs

local mobslist = {
	-- friendlies
	"pig",
	"panda",
	"tortoise",
	"golem_friendly",
	"gnorm",
	"hedgehog",
	"owl",
	"whale",
	"badger",
	"butterfly",
	"elephant",

	-- baddies
	"pig_evil",
	"fox",
	"rat",
	"wasps",
	"treeman",
	"golem",
	"skeleton",
	"orc",
	"ogre",
}

minetest.log("action","[mobs_doomed] nyancat enabled "..tostring(mobs_doomed.allow_nyanc))
minetest.log("action","[mobs_doomed] dragons enabled "..tostring(mobs_doomed.dragons))
minetest.log("action","[mobs_doomed] more animals "..tostring(mobs_doomed.regulars))
minetest.log("action","[mobs_doomed] enable destructive fire "..tostring(mobs_doomed.destructive))
minetest.log("action","[mobs_doomed] configured egg timer "..tostring(mobs_doomed.eggtimer))
if mobs_doomed.regulars then
	for _,mobname in pairs(mobslist) do
		loadmob(mobname)
	end
	if mobs_doomed.allow_nyanc then
		loadmob("nyan")
	end
end

---------------
-- dragons!! --
---------------

dofile(minetest.get_modpath("mobs_doomed").."/dragons/piloting.lua")
loadmob("dragon_normal","/dragons/")
if mobs_doomed.dragons then
	loadmob("main","/dragons/")
	loadmob("dragon1","/dragons/")
	loadmob("dragon2","/dragons/")
	loadmob("dragon3","/dragons/")
	loadmob("dragon4","/dragons/")
	loadmob("great_dragon","/dragons/")
	loadmob("water_dragon","/dragons/")
	loadmob("wyvern","/dragons/")

	dofile(minetest.get_modpath("mobs_doomed").."/dragons/eggs.lua")
end
dofile(minetest.get_modpath("mobs_doomed").."/arrows/dragonfire.lua")
dofile(minetest.get_modpath("mobs_doomed").."/arrows/dragonarrows.lua")

-- General arrow definitions

if mobs_doomed.destructive == true then
	dofile(minetest.get_modpath("mobs_doomed").."/arrows/fire_explosive.lua")
else
	dofile(minetest.get_modpath("mobs_doomed").."/arrows/fire.lua")
end

dofile(minetest.get_modpath("mobs_doomed").."/nodes.lua")
dofile(minetest.get_modpath("mobs_doomed").."/arrows/sting.lua")

-- Spawning


dofile(minetest.get_modpath("mobs_doomed").."/spawn.lua")
dofile(minetest.get_modpath("mobs_doomed").."/saddle.lua")

print("[MOD] mobs_doomed loaded")
