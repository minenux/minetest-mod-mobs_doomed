* 2023-06-04 : mckaygerhard
  * dont relly on the legacy layer of default mod, detect if player api is present
  * remove any conflicts with lib_mount
  * fix fox behaviour, fix water dragon aim
  * fix wrong local non local variable for function scope only
  * sync with tenplus1 last changes, backport fixeds
* 2022-01-23 : mckaygerhard
  * change namespace from dmobs to mobs_doomed
  * fixed depends
  * fixed damage by fire
  * improvements over readme and information, added screenshot
  * Fixed a few nil error bugs from tenplus1 and more checks
* 2021-03-30 : Poikilos
  * Add more details to the panda texture (Change every pixel give or take a few) while reducing the number of pixels.
* 2019-07-31 : Grossam
  * spawn.lua rewritten using mobs:spawn()
  * spawn rules modified for more consistency (e.g. hedgehogs now spawn at night)
  * some sounds and textures added
  * dragon arrow bug corrected
  * elemental an great drangons can fly in water (still take damages from it !)
  * great dragons may spawn in caverealms
* 2019-07-28 : Grossam
  * some little bugs corrected
  * dragon_normal described as 'minor dragons' and now they can spawn
  * ice dragons are now named "Blue dragon"
  * hatched great dragons are tamed now
  * great dragons now need a diamond block nest (you've to deserve greatness !)
* 2019-07-27 : Grossam
  * Forked on https://git.fwhost.eu/Grossam/dmobs

