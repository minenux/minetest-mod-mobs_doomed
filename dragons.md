minetest mod mobs_doomed
======================

MOBS for dragons.. Main info at [Readme.md](README.md)

## Dragns and orcs/ogres

When dragons are disabled orcs will spawm more frecuently, 
about 16000 chance and 32000 (very rare) chance if dragons are disabled, 
same for ogres, about 6000 with disabled dragons and 8000 then.

## Dragons names


Generic dragon always spawn, the others only if enabled in settings by `mobs_doomed.dragons` set to true (default):

| tech name     | type | chance | notes              | spawn nodes            |
| ------------- | ---- | ------ | ------------------ | ---------------------- |
| mobs_doomed:dragon  | mob  | 16000  | always enabled     | group:leaves           |
| mobs_doomed:waterdragon | mob  | 24000 | rare, ice/water  | default:water_source |
| mobs_doomed:dragon1 | mob  | 24000  | rare, fire | ethereal:fiery_dirt, default:desert_sand |
| mobs_doomed:dragon2 | mob  | 24000  | rare, thunder/fire | ethereal:cristal_dirt, default:dirt_with_dry_grass |
| mobs_doomed:dragon3 | mob  | 30000  | rare, poison | ethereal:jungle_dirt, default:jungleleaves |
| mobs_doomed:dragon4 | mob  | 24000  | rare, ice/water | default:snow_block, default:permafrost_with_moss, default:permafrost_with_stone, ethereal:cold_dirt |
| mobs_doomed:wyvern  | mob  | 32000  | rare, dog attack | group:leaves |
| mobs_doomed:dragon_great | mob  | 30000 | very rare, fire/lava | ethereal:jungle_dirt,default:jungleleaves,default:lava_source,caverealms:glow_mese, caverealms:glow_amethyst,caverealms:glow_crystal,caverealms:glow_emerald,cavereals:glow_ruby |
| mobs_doomed:dragon_great_tame | npc | 30000 | very rare, fire/lava | ethereal:jungle_dirt,default:jungleleaves,default:lava_source,caverealms:glow_mese, caverealms:glow_amethyst,caverealms:glow_crystal,caverealms:glow_emerald,cavereals:glow_ruby |

Dragon D00Med's Mobs
-------------

![screenshot_red_dragon.png](screenshot_red_dragon.png)

## How to Dragon

By defeating dragons you will get eggs. Place them in their ideal environment and feed them a dragon gem (the violet one) to create an 
elemental egg.

Environements :

* Fire : lava source
* Lightning : obsidian
* Poison : cactus
* Ice : ice
* Great : diamond block

By defeating special dragons, you can obtain elemental gems.

Feed the right elemental gem to the elemental egg to hatch a dragon:
* Fire : fire gem
* Lightning : lightning gem
* Poison : poison gem
* Ice : ice gem
* Great : dragon gem

Use the Use key to shoot fireballs when riding your new pet dragon.
Press the forward key to fly to the direction you're looking

## Configurations

there's a variable named `mobs_doomed.dragons` by default set to true, if disabled or false will spawn more orcs 
and only let the `mobs_doomed:dragon` available for spawn. This can be set using the 
config file (server) or using the gui configure advanced part from the client (single player)

Enjoy !

Main info at [Readme.md](README.md)

