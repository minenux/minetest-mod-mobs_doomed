-- spawn.lua rewritten, using this latest function frim mobs_redo :
--
--mobs:spawn({
--            name = "mobs_doomed:dragon",
--            nodes = {"air"},
--            neighbor = {"default:stone"},
--            min_light = 10,
--            max_light = 15,
--            interval = 300,
--            chance = 15000,
--            active_object_count = 2,
--            min_height = -100,
--            max_height = 11000,
--})
--
-- The fonction creates a rule defining how a mob will spawn.
-- Call it several times to create several rules.


-- mobs:spawn() seems to ignore unloaded nodes. So no need checking for loaded mods ?
--local eth = minetest.get_modpath("ethereal")
--local cr = minetest.get_modpath("caverealms")
--local nodes, neighbor = {}, {}

if mobs_doomed.regulars then
	-- friendlies
--	nodes = {"darkage:marble"}
--	neighbor = {"darkage:slate"}
	if mobs_doomed.allow_nyanc then
		mobs:spawn({name = "mobs_doomed:nyan", nodes = {"air"}, neighbor = {"group:leaves"},
			min_light = 10, max_light = 15, interval = 300, chance = 64000, active_object_count = 2, min_height = 0, max_height = 2000})
		mobs:spawn({name = "mobs_doomed:nyan", nodes = {"air"}, neighbor = {"ethereal:mushroom", "nyanland:meseleaves"},
			min_light = 10, max_light = 15, interval = 300, chance = 16000, active_object_count = 2, min_height = 0, max_height = 2000})
	end
	mobs:spawn({name = "mobs_doomed:hedgehog", nodes = {"group:flora", "ethereal:prairie_dirt"},
		neighbor = {}, min_light = 0, max_light = 8, interval = 300, chance = 8000, active_object_count = 3, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:whale", nodes = {"default:water_source"}, neighbor = {"group:sand"},
		min_light = 0, max_light = 15, interval = 300, chance = 16000, active_object_count = 2, min_height = -100, max_height = 0})
	mobs:spawn({name = "mobs_doomed:owl", nodes = {"group:tree"}, neighbor = {},
		min_light = 0, max_light = 7, interval = 300, chance = 16000, active_object_count = 2, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:gnorm", nodes = {"default:dirt_with_grass", "ethereal:bamboo_dirt"}, neighbor = {},
		min_light = 10, max_light = 15, interval = 300, chance = 32000, active_object_count = 2, min_height = -100, max_height = 0})
	mobs:spawn({name = "mobs_doomed:tortoise", nodes = {"default:water_source", "group:sand"}, neighbor = {},
		min_light = 5, max_light = 10, interval = 300, chance = 8000, active_object_count = 2, min_height = -100, max_height = 500})
	mobs:spawn({name = "mobs_doomed:elephant", nodes = {"default:dirt_with_dry_grass", "ethereal:grove_dirt"}, neighbor = {},
		min_light = 10, max_light = 15, interval = 300, chance = 16000, active_object_count = 2, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:pig", nodes = {"default:dirt_with_grass", "ethereal:prairie_dirt", "nyanland:cloudstone"}, neighbor = {},
		min_light = 10, max_light = 15, interval = 300, chance = 32000, active_object_count = 2, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:panda", nodes = {"default:dirt_with_grass", "ethereal:bamboo_dirt"}, neighbor = {},
		min_light = 10, max_light = 15, interval = 300, chance = 32000, active_object_count = 2, min_height = 0, max_height = 2000})


	-- baddies

	mobs:spawn({name = "mobs_doomed:wasp", nodes = {"air"}, neighbor = {"group:leaves"}, min_light = 10, max_light = 15, interval = 300, chance = 32000, active_object_count = 2, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:wasp", nodes = {"mobs_doomed:hive"}, neighbor = {}, min_light = 10, max_light = 15, interval = 300, chance = 16000, active_object_count = 2, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:wasp_leader", nodes = {"group:leaves", "mobs_doomed:hive"}, neighbor = {}, min_light = 10, max_light = 15, interval = 300, chance = 64000, active_object_count = 2, min_height = 0, max_height = 2000})

	mobs:spawn({name = "mobs_doomed:golem", nodes = {"group:stone"}, neighbor = {}, min_light = 0,
		max_light = 7, interval = 300, chance = 16000, active_object_count = 2, min_height = -32000, max_height = 100})
	mobs:spawn({name = "mobs_doomed:pig_evil", nodes = {"group:leave", "ethereal:bamboo_leaves"}, neighbor = {},
		min_light = 10, max_light = 15, interval = 300, chance = 64000, active_object_count = 2, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:fox", nodes = {"group:leaves"}, neighbor = {},
		min_light = 0, max_light = 10, interval = 300, chance = 32000, active_object_count = 2, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:rat", nodes = {"group:stone", "group:sand"}, neighbor = {},
		min_light = 0, max_light = 10, interval = 300, chance = 32000, active_object_count = 2, min_height = -30000, max_height = 100})
	mobs:spawn({name = "mobs_doomed:treeman", nodes = {"group:leaves"}, neighbor = {},
		min_light = 7, max_light = 15, interval = 300, chance = 16000, active_object_count = 2, min_height = 0, max_height = 2000})
	mobs:spawn({name = "mobs_doomed:skeleton", nodes = {"group:stone","caverealms:stone_with_salt","default:desert_sand"}, neighbor = {},
		min_light = 0, max_light = 10, interval = 300, chance = 16000, active_object_count = 2, min_height = -31000, max_height = -1000})

-- Orcs and ogres spawn more often when dragons are disabled
	if not mobs_doomed.dragons then
		mobs:spawn({name = "mobs_doomed:orc",
			nodes = {"default:snow_block", "default:permafrost_with_moss", "default:permafrost_with_stone", "ethereal:cold_dirt"},
			neighbor = {}, min_light = 0, max_light = 10, interval = 300, chance = 6000,
			active_object_count = 2, min_height = 0, max_height = 2000})
		mobs:spawn({name = "mobs_doomed:ogre",
			nodes = {"default:snow_block", "default:permafrost_with_moss", "default:permafrost_with_stone", "ethereal:cold_dirt"},
			neighbor = {}, min_light = 0, max_light = 10, interval = 300, chance = 16000,
			active_object_count = 2, min_height = 0, max_height = 2000})
	else
		mobs:spawn({name = "mobs_doomed:orc",
			nodes = {"default:snow_block", "default:permafrost_with_moss", "default:permafrost_with_stone", "ethereal:cold_dirt"},
			neighbor = {}, min_light = 0, max_light = 10, interval = 300, chance = 8000,
			active_object_count = 2, min_height = 0, max_height = 2000})
		mobs:spawn({name = "mobs_doomed:ogre",
			nodes = {"default:snow_block", "default:permafrost_with_moss", "default:permafrost_with_stone", "ethereal:cold_dirt"},
			neighbor = {}, min_light = 0, max_light = 10, interval = 300, chance = 32000,
			active_object_count = 2, min_height = 0, max_height = 2000})
	end
end


-------------
-- dragons --
-------------

-- Generic dragon always spawn, the others only if enabled
mobs:spawn({name = "mobs_doomed:dragon", nodes = {"group:leaves"}, neighbor = {},
	min_light = 5, max_light = 15, interval = 300, chance = 16000, active_object_count = 2, min_height = 0, max_height = 30000})

if mobs_doomed.dragons then
	mobs:spawn({name = "mobs_doomed:dragon1", nodes = {"ethereal:fiery_dirt", "default:desert_sand"}, neighbor = {},
		min_light = 5, max_light = 15, interval = 300, chance = 24000, active_object_count = 2, min_height = 0, max_height = 30000})
	mobs:spawn({name = "mobs_doomed:dragon2", nodes = {"ethereal:crystal_dirt", "default:dirt_with_dry_grass"}, neighbor = {},
		min_light = 5, max_light = 15, interval = 300, chance = 24000, active_object_count = 2, min_height = 0, max_height = 30000})
	mobs:spawn({name = "mobs_doomed:dragon3", nodes = {"ethereal:jungle_dirt", "default:jungleleaves"}, neighbor = {},
		min_light = 0, max_light = 10, interval = 300, chance = 24000, active_object_count = 2, min_height = 0, max_height = 30000})
	mobs:spawn({name = "mobs_doomed:dragon4",
		nodes = {"default:snow_block", "default:permafrost_with_moss", "default:permafrost_with_stone", "ethereal:cold_dirt"}, neighbor = {},
		min_light = 5, max_light = 15, interval = 300, chance = 24000, active_object_count = 2, min_height = 0, max_height = 30000})
	mobs:spawn({name = "mobs_doomed:waterdragon", nodes = {"default:water_source"}, neighbor = {"air"},
		min_light = 0, max_light = 15, interval = 300, chance = 32000, active_object_count = 2, min_height = -10, max_height = 100})
	mobs:spawn({name = "mobs_doomed:wyvern", nodes = {"group:leaves"}, neighbor = {},
		min_light = 0, max_light = 10, interval = 300, chance = 32000, active_object_count = 2, min_height = 0, max_height = 30000})
	mobs:spawn({name = "mobs_doomed:dragon_great",
		nodes = {"ethereal:jungle_dirt", "default:jungleleaves", "default:lava_source", "caverealms:glow_mese",
			"caverealms:glow_amethyst", "caverealms:glow_crystal", "caverealms:glow_emerald","cavereals:glow_ruby"}, neighbor = {},
		min_light = 0, max_light = 15, interval = 300, chance = 32000, active_object_count = 2, min_height = -30000, max_height = 30000})
end
