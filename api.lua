--vehicles/mounts api by D00Med, based on lib_mount(see below)

--License of lib_mount:
-- Minetest mod: lib_mount
-- =======================
-- by blert2112

-- Based on the Boats mod by PilzAdam.


-- -----------------------------------------------------------
-- -----------------------------------------------------------


-- Minetest Game mod: boats
-- ========================
-- by PilzAdam

-- License of source code:
-- -----------------------
-- WTFPL


--from lib_mount (required by new functions)

local p_api = minetest.get_modpath("player_api")
local mobs_redo = false
if mobs.mod and mobs.mod == "redo" then
	mobs_redo = true
end

local function is_group(pos, group)
	local nn = minetest.get_node(pos).name
	return minetest.get_item_group(nn, group) ~= 0
end

local function force_detach(player)
	local attached_to = player:get_attach()
	if attached_to and attached_to:get_luaentity() then
		local entity = attached_to:get_luaentity()
		if entity.driver then
			entity.driver = nil
		end
		player:set_detach()
	end
	if p_api then player_api.player_attached[player:get_player_name()] = false
	else default.player_attached[player:get_player_name()] = false end
	player:set_eye_offset({x=0, y=0, z=0}, {x=0, y=0, z=0})
end

function mobs_doomed.object_attach(entity, player, attach_at, eye_offset)
	eye_offset = eye_offset or {x=0, y=0, z=0}
	force_detach(player)
	entity.driver = player
	player:set_attach(entity.object, "", attach_at, {x=0, y=0, z=0})
	
	player:set_properties({visual_size = {x=1, y=1}})
	
	player:set_eye_offset(eye_offset, {x=0, y=0, z=0})
	if p_api then player_api.player_attached[player:get_player_name()] = true
	else default.player_attached[player:get_player_name()] = true end

	minetest.after(0.2, function()
		if p_api then player_api.set_animation(player, "sit" , 30)
		else default.player_set_animation(player, "sit" , 30) end
	end)
	--entity.object:set_yaw(player:get_look_yaw() - math.pi / 2)
	entity.object:set_yaw(player:get_look_horizontal() - (math.pi/2))
end

function mobs_doomed.object_detach(entity, player, offset)
	entity.driver = nil
	player:set_detach()
	if p_api then
		player_api.player_attached[player:get_player_name()] = false
		player_api.set_animation(player, "stand" , 30)
	else
		default.player_attached[player:get_player_name()] = false
		default.player_set_animation(player, "stand" , 30)
	end
	player:set_eye_offset({x=0, y=0, z=0}, {x=0, y=0, z=0})
	local pos = player:get_pos()
	pos = {x = pos.x + offset.x, y = pos.y + 0.2 + offset.y, z = pos.z + offset.z}
	minetest.after(0.1, function()
		player:set_pos(pos)
	end)
end
-------------------------------------------------------------------------------


minetest.register_on_leaveplayer(function(player)
	if player then
		force_detach(player)
	end
end)

minetest.register_on_shutdown(function()
	local playerc
	local players = minetest.get_connected_players()
	for i = 1,#players do
		playerc = players[i]
		if playerc then
			force_detach(players[i])
		end
		playerc = nil
	end
	
end)

minetest.register_on_dieplayer(function(player)
	if player then
		force_detach(player)
		return true
	end
end)

-------------------------------------------------------------------------------

--mixed code(from this mod and lib_mount)
local rotview = math.pi / 2

function mobs_doomed.object_drive(entity, dtime, speed, shoots, arrow, moving_anim, stand_anim, jumps)
	local ctrl = entity.driver:get_player_control()
	local dir = entity.driver:get_look_dir();
	local vec_forward = {x=dir.x*speed,y=-2,z=dir.z*speed}
	local vec_backward = {x=-dir.x*speed,y=-2,z=-dir.z*speed}
	local vec_stop = {x=0,y=0,z=0}
	--local yaw = entity.driver:get_look_yaw()
	local yaw = entity.driver:get_look_horizontal()
	if ctrl.up then
		entity.object:set_yaw(yaw+math.pi+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
		entity.object:set_velocity(vec_forward)
	elseif ctrl.down then
		entity.object:set_yaw(yaw+math.pi+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
		entity.object:set_velocity(vec_backward)
	elseif not ctrl.down or ctrl.up then
		entity.object:set_yaw(yaw+math.pi+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
		entity.object:set_velocity(vec_stop)
	end
	if ctrl.sneak and ctrl.LMB and shoots then
			local pos = entity.object:get_pos()
			local obj = minetest.add_entity({x=pos.x+0+dir.x*2,y=pos.y+1.5+dir.y,z=pos.z+0+dir.z*2}, arrow)
			local vec = {x=dir.x*6,y=dir.y*6,z=dir.z*6}
			local yaw = entity.driver:get_look_horizontal() + rotview -- we have math.pi/2 but later yaw sumarized again in next line
			obj:set_yaw(yaw+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
			obj:set_velocity(vec)
	end
	--lib_mount animation
	local velo = entity.object:get_velocity()
	if velo.x == 0 and velo.y == 0 and velo.z == 0 then
		if stand_anim and stand_anim ~= nil and mobs_redo == true then
			mobs:set_animation(entity, stand_anim)
		end
		entity.object:set_pos(entity.object:get_pos())
		return
	end
	if moving_anim and moving_anim ~= nil and mobs_redo == true then
		mobs:set_animation(entity, moving_anim)
	end
	--jumping not working
	-- local jumps = jumps or false
	-- if jumps == true and ctrl.jump then
		-- local vel = entity.object:get_velocity()
		-- vel.y = 10
		-- entity.object:set_velocity(vel)
		-- minetest.after(1.5, function()
		--  local vel = entity.object:get_velocity()
		--  vel.y = -10
		--  entity.object:set_velocity(vel)
		-- end)
	-- end
end

function mobs_doomed.object_fly(entity, dtime, speed, shoots, arrow, moving_anim, stand_anim)

	if entity and not entity.driver:get_look_dir() then return end

	local ctrl = entity.driver:get_player_control()
	local velo = entity.object:get_velocity()
	local dir = entity.driver:get_look_dir();
	local vec_forward = {x=dir.x*speed,y=dir.y*speed+2,z=dir.z*speed}
	local vec_backward = {x=-dir.x*speed,y=dir.y*speed+2,z=-dir.z*speed}
	local vec_rise = {x=velo.x,y=velo.y+0.2,z=velo.z}
	local vec_stop = {x=0,y=-0.2,z=0}
	local yaw = entity.driver:get_look_horizontal() + rotview
	local pos = entity.object:get_pos()
	local node = minetest.get_node(pos).name

-- Commented condition makes dragons stuck in water, lava and so on…
--	if node == "default:water_source" or node == "default:river_water_source" or node == "default:river_water_flowing" or node == "default:water_flowing" or node == "default:lava_source" or node == "default:lava_flowing" then
--		entity.object:set_velocity({x=velo.x*0.5, y=velo.y*0.5, z=velo.z*0.5})
--	end
	if ctrl.up then
		entity.object:set_yaw(yaw+math.pi+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
		entity.object:set_velocity(vec_forward)
	elseif ctrl.down then
		entity.object:set_yaw(yaw+math.pi+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
		entity.object:set_velocity(vec_backward)
	elseif ctrl.jump then
		entity.object:set_yaw(yaw+math.pi+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
		entity.object:set_velocity(vec_rise)
	elseif not ctrl.down or ctrl.up or ctrl.jump then
		entity.object:set_yaw(yaw+math.pi+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
		entity.object:set_velocity(vec_stop)
	end
	if ctrl.aux1 and shoots and not entity.loaded then
		local pos = entity.object:get_pos()
		local obj = minetest.add_entity({x=pos.x+0+dir.x*2.5,y=pos.y+1.5+dir.y,z=pos.z+0+dir.z*2.5}, arrow)
		local vec = vector.multiply(dir, 12)
		--local yaw = entity.driver:get_look_yaw()
		local yaw = entity.driver:get_look_horizontal()
		entity.loaded = true
		obj:set_yaw(yaw+math.pi/2) -- check math.pi+math.pi/2 check if the order of the signs is correct
		obj:set_velocity(vec)
		local object = obj:get_luaentity()
		object.launcher = entity.driver
		minetest.after(1, function()
			entity.loaded = false
		end)
	end
	--lib_mount animation
	if velo.x == 0 and velo.y == 0 and velo.z == 0 then
		if stand_anim and stand_anim ~= nil and mobs_redo == true then
			mobs:set_animation(entity, stand_anim)
		end
		entity.object:set_pos(entity.object:get_pos())
		return
	end
	if moving_anim and moving_anim ~= nil and mobs_redo == true then
		mobs:set_animation(entity, moving_anim)
	end
end


--lib_mount (not required by new functions)

local function is_group(pos, group)
	local nn = minetest.get_node(pos).name
	return minetest.get_item_group(nn, group) ~= 0
end

local function get_sign(i)
	i = i or 0
	if i == 0 then
		return 0
	else
		return i / math.abs(i)
	end
end

local function get_velocity(v, yaw, y)
	local x = -math.sin(yaw) * v
	local z =  math.cos(yaw) * v
	return {x = x, y = y, z = z}
end

local function get_v(v)
	return math.sqrt(v.x ^ 2 + v.z ^ 2)
end

