minetest mod mobs D00Med
=========================

MOBS for dragons, pandas, orcs, ogres, fox, owl, and many more animals that can be loved or enemies!

Information
-----------

Some of those mobs are already in others mods.. like Dragon (nether), wasp (nssm/mobs_monster)

Dragon gems are dropped by the "boss" dragons ie: the wyvern, the water dragon, and the great dragon(red+black).

If you see a wasp, bee careful because it wasp probably spawned by the "king of sting", a huge wasp that will make nests and spawn more wasps. Golems sometimes drop their heads. you can use these to make a friendly golem, if you place one on top of two stone blocks. Foxes are sneaky, they will dig through fences if you try to trap them.

![](screenshot.png)

You must first place an egg in a "nest" made of lava, obsidian, cactus, or ice.
Then you can right click the egg with a gem(the purple one), and after a while the egg will change color. Then you have to right click it with the gem that matches the type of dragon in the egg.

* Red dragon - Lava nest - Fire gem
* Green dragon - Cactus nest - Poison gem
* Blue dragon - Ice nest - Ice gem
* Black dragon - Obsidian nest - Lightning gem

The nests are just the nodes above, surrounding the egg on all sides and underneath.

To get tame dragons you need to hatch them from eggs.

Technical info
--------------

This mod is named `mobs_doomed` in this branch, and includes all the changes from the tenplus1 fork and 
some improvements fixeds from mckaygerhard namespaced changes. Its the same from `dmobs` but plus all the 
recent changes and also fixeds, with renamed namespace to be named as `mobs_doomed`.

This means its compatible with all engine releases of minetest since 0.4 to 5.5 
and maybe latest.

> **Warning** **This mod will added 10 megs of more media to yuor server game so clients on mobile may sufering**

Please for more technical info about dragons read the [dragons.md](dragons.md) file.

There's an api for vehicles and mount, it forked little **lib_mount lua** api so can be outdated in future.

Models sources are in [projects](projects) directory of course in GIMP format.

### Depends

* default
* mobs
* wool
* farming
* bones
* mobs_animal (optional)
* ethereal (optional)
* caverealms (optional)
* player_api (this depends if you use older game base)

### Download

This is a fork that tries to rename the namespace to `mobs_doomed` from `dmobs` o be in sync with mobs api from tenplus1.
to use the older named mod just download it but before commit https://codeberg.org/minenux/minetest-mod-mobs_doomed/commit/7910cbf5dd9ce97c72c056ee656966b32cc1b574 .

To use the `mobs_doomed` (that is same as tenplus1 dmobs and includes the compatiblity from original one) just 
download this current branch and be sure to named the directory mod as `mobs_doomed`.

It also try to be compatible with both 0.4 and 5.2 engines of minetest for minenux games. This fork can be downloaded 
from https://codeberg.org/minenux/minetest-mod-mobs_doomed the original one is at https://github.com/minetest-mobs-mods/dmobs

### Configurations

| Configuration variable  | Description                    | type  | default |
| ----------------------- | ------------------------------ | ----- | ----- |
| mobs_doomed.regulars    | Enable most common new animals | bool  | true  |
| mobs_doomed.eggtimer    | Timer for the egg mechanics    | int   | 100   |
| mobs_doomed.destructive | Fire balls will be destructive | bool  | true  |
| mobs_doomed.dragons     | If disabled only a common dragon will be spawned | bool | true |
| mobs_doomed.allow_nyanc | Enable the stupid NyanCat mob fliying pig | bool | false |

### mobs nodes and names

Whit more chance, more rare.

There's some settings, if `mobs_doomed.regulars` are enabled then those monster will be available:

| tech name      | type  | chance  | notes      | spawn nodes                 |
| -------------- | ----- | ------- | ---------- | --------------------------- |
| mobs_doomed:nyan     | mobs  | 300     | friendly   | air or ethereal:mushroom/nyanland:meseleaves |
| mobs_doomed:hedgehog | mobs  | 8000    | friendly   | group:flora, ethereal:prairie_dirt |
| mobs_doomed:whale    | mobs  | 16000   | friendly   | default:water_source        |
| mobs_doomed:owl      | mobs  | 16000   | friendly   | group:tree                  |
| mobs_doomed:gnorm    | npc   | 16000   | friendly   | default:dirt_with_grass, ethereal:bamboo_dirt  |
| mobs_doomed:tortoise | mobs  | 8000    | friendly   | default:water_source, group:sand |
| mobs_doomed:elephan  | mobs  | 16000   | friendly   | default:dirt_with_dry_grass, ethereal:grove_dirt |
| mobs_doomed:pig      | mobs  | 32000   | friendly   | default:dirt_with_grass, ethereal:prairie_dirt, nyanland:cloudstone |
| mobs_doomed:panda    | mobs  | 32000   | friendly   | default:dirt_with_grass, ethereal:bamboo_dirt |
| mobs_doomed:waps     | mobs  | 16000   | baddly     | air/leaves  or mobs_doomed:hive   |
| mobs_doomed:waps_leader | mobs | 64000 | baddly     | air/leaves  or mobs_doomed:hive   |
| mobs_doomed:golem    | mobs  | 16000   | baddly     | group:stone                 |
| mobs_doomed:pig_evil | mobs  | 64000   | baddly     | group:leave, ethereal:bamboo_leaves   |
| mobs_doomed:fox      | mobs  | 32000   | baddly     | group:leaves                |
| mobs_doomed:rat      | mobs  | 32000   | baddly     | group:stone, group:sand     |
| mobs_doomed:treeman  | mobs  | 16000   | baddly     | group:leaves                |
| mobs_doomed:skeleton | mobs  | 16000   | baddly     | group:stone, caverealms:stone_with_salt, default:desert_sand |
| mobs_doomed:badger   | mobs  | 8000    | friendly   | |

There's some settings, if `mobs_doomed.dragon`, orcs and ogres spawn more often when dragons are disabled:

| tech name      | type  | chance  | notes      | spawn nodes                 |
| -------------- | ----- | ------- | ---------- | --------------------------- |
| mobs_doomed:orc      | mobs  | 8000/2000 | baddly     | default:snow_block, default:permafrost_with_moss, default:permafrost_with_stone, ethereal:cold_dirt |
| mobs_doomed:ogre     | mobs  | 16000/32000 | baddly     | default:snow_block, default:permafrost_with_moss, default:permafrost_with_stone, ethereal:cold_dirt |
| mobs_doomed:dragon_great_tame | npc | 30000 | very rare, fire/lava | ethereal:jungle_dirt,default:jungleleaves,default:lava_source,caverealms:glow_mese, caverealms:glow_amethyst,caverealms:glow_crystal,caverealms:glow_emerald,cavereals:glow_ruby 
| mobs_doomed:dragon   | mobs  | 16000  | always enabled     | group:leaves           |
| mobs_doomed:waterdragon | mobs  | 24000 | rare, ice/water  | default:water_source |
| mobs_doomed:dragon1 | mobs  | 24000  | rare, fire | ethereal:fiery_dirt, default:desert_sand |
| mobs_doomed:dragon2 | mobs  | 24000  | rare, thunder/fire | ethereal:cristal_dirt, default:dirt_with_dry_grass |
| mobs_doomed:dragon3 | mobs  | 30000  | rare, poison | ethereal:jungle_dirt, default:jungleleaves |
| mobs_doomed:dragon4 | mobs  | 24000  | rare, ice/water | default:snow_block, default:permafrost_with_moss, default:permafrost_with_stone, ethereal:cold_dirt |
| mobs_doomed:wyvern  | mobs  | 32000  | rare, dog attack | group:leaves |
| mobs_doomed:dragon_great | mobs  | 30000 | very rare, fire/lava | ethereal:jungle_dirt,default:jungleleaves,default:lava_source,caverealms:glow_mese, caverealms:glow_amethyst,caverealms:glow_crystal,caverealms:glow_emerald,cavereals:glow_ruby |


## Authors & License

Thanks to D00Med, TenPlus1, blert2112, Grossam and taikedz

See [license.txt](license.txt)
